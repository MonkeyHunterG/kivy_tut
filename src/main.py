from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.graphics import Color
from kivy.graphics import Rectangle
from kivy.graphics import Line
from kivy.graphics import Point


class Touch(Widget):
    rect_size = 50

    def __init__(self, **kwargs):
        super(Touch, self).__init__(**kwargs)
        self.rect = Rectangle(pos=(0, 0), size=(self.rect_size, self.rect_size))

        with self.canvas:
            Color(255, 0, 0, mode='rgb')
            self.rect = Rectangle(pos=(0, 0), size=(self.rect_size, self.rect_size))
            Color(255, 0, 0, mode='rgb')
            Line(points=(0, 0, 100, 100, 600, 300), width=5, joint='round')
            Color(255, 0, 0, mode='rgb')
            Point(points=[500, 500], pointsize=10)

    def on_touch_down(self, touch):
        self.rect.pos = touch.pos
        # Retain all on click functionality from other widgets
        super().on_touch_down(touch)

    def on_touch_move(self, touch):
        self.rect.pos = touch.pos
        # Retain all on click functionality from other widgets
        super().on_touch_move(touch)

    def on_touch_up(self, touch):
        # Retain all on click functionality from other widgets
        super().on_touch_up(touch)


class MyGrid(Widget):
    # The variable names have to be identical to
    # the global variable names in the kv file
    name = ObjectProperty(None)
    age = ObjectProperty(None)

    def pressed(self):
        name = self.name.text
        age = self.age.text
        print("Hello, %s. You are %s" % (name, age))
        self.name.text = ""
        self.age.text = ""


class MyGridApp(App):
    def build(self):
        return MyGrid()


class MyFloatApp(App):
    def build(self):
        return FloatLayout()


class MyTouchApp(App):
    def build(self):
        return Touch()


# Either MyGridApp().run() or MyFloatApp().run() or MyTouchApp().run()
if __name__ == "__main__":
    MyTouchApp().run()
